import 'package:flutter/material.dart';

class DropDownWidget extends StatefulWidget {
  DropDownWidget({Key? key}) : super(key: key);

  @override
  _DropDownWidgetState createState() => _DropDownWidgetState();
}

class _DropDownWidgetState extends State<DropDownWidget> {
  String vaccine = '-';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('DropDown'),
      ),
      body: Column(
        children: [
          Container(
            child: Row(
              children: [
                Text('Vaccine: '),
                Expanded(child: Container()),
                DropdownButton(
                  items: [
                    DropdownMenuItem(child: Text('-'), value: '-'),
                    DropdownMenuItem(child: Text('Pfizer'), value: 'Pfizer'),
                    DropdownMenuItem(
                        child: Text('Johnson & Johnson'), value: 'Johnson & Johnson'),
                    DropdownMenuItem(
                        child: Text('SputnikV'), value: 'SputnikV'),
                    DropdownMenuItem(
                        child: Text('Astrazeneca'), value: 'Astrazeneca'),
                    DropdownMenuItem(child: Text('Novavax'), value: 'Novavax'),
                    DropdownMenuItem(
                        child: Text('Sinopharm'), value: 'Sinopharm'),
                    DropdownMenuItem(child: Text('Sinovax'), value: 'Sinovax'),
                  ],
                  value: vaccine,
                  onChanged: (String? newValue) {
                    setState(() {
                      vaccine = newValue!;
                    });
                  },
                )
              ],
            ),
          ),
          Center(
            child: Text(
              vaccine,
              style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
            ),
          )
        ],
      ),
    );
  }
}
